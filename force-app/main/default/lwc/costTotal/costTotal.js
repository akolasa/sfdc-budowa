import {LightningElement, track, wire} from 'lwc';
import getCostTotal from '@salesforce/apex/CostTotalController.getCostTotal';

export default class CostTotal extends LightningElement {

    @track totalCosts;
    @track error;

    connectedCallback() {
        getCostTotal()
            .then(result => {
                console.log('result: ',result)
                this.totalCosts = result;
            })
            .catch(error => {
                this.error = error;
            });
    }
}