public with sharing class CostTotalController {

    @AuraEnabled
    public static Decimal getCostTotal(){
        List<AggregateResult> costs = [SELECT SUM(Koszt__c) FROM Cost__c];
        return (Decimal) costs[0].get('expr0');
    }
}